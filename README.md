# E02 Hola Mundo Mario 3NES
Hola Mundo inspirado en Super Mario Bros. 3 de Nintendo.
* El Hola mundo contendrá en un menú lateral izquierdo los botones, de tipo de selección opcional, para saludar a los 8 Mundos existentes en esta versión de Mario.
* Arriba de los botones, estará una imagen de Mario o de la portada del juego, y aparte habrá un botón de adiós al final.
* Cada botón hará que, en el espacio de la derecha, se muestre el texto de Hola Mundo y el número de mundo, con una imagen del mundo correspondiente de fondo.
* El botón de fin, llevará a una imagen de Mario encontrando a Peach luego de derrotar a Bowser.
* Las imágenes e íconos propuestas pueden descargarse de este repositorio en la carpeta recursos.
